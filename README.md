# Shopping Mall API

A simple e-commerce website

## Description

An e-commerce API built with Nestjs

## Postman

[Postman collection](https://www.postman.com/altimetry-operator-94923191/workspace/shopping-mall-app/collection/17462888-e00060d7-68a6-4a6e-8616-b619716d5abb?action=share&creator=17462888)

## Authors

Contributors names and contact info

- Ebueku Uyiosa Peter  
  [LinkedIn](https://www.linkedin.com/in/pebueku)  
  [Dev.to](https://dev.to/hackermanpeter)
